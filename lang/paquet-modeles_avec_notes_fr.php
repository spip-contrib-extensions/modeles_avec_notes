<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'modeles_avec_notes_description' => 'Plugin permettant de créer des modèles générant des notes avec page.',
	'modeles_avec_notes_slogan' => 'Des notes (semi-)automatiques',
);
