<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Recherche les modèles
 * Et, si jamais ils sont renvoyés par le pipeline modeles_avec_notes
 * les inteprètes
 * ceci AVANT que propre soit passés
 * ce qui permettra d'interprète les notes
 * @param string $flux;
 * @return string $flux modifié.
**/
function modeles_avec_notes_pre_liens($flux) {
	include_spip('inc_liens');
	include_spip('public/assembler');
	include_spip('src/Texte/Collecteur/Modeles');
	if (class_exists('\SPIP\Texte\Collecteur\Modeles')) {
		$modele = new \SPIP\Texte\Collecteur\Modeles();
		$matches = $modele->collecter($flux);
		$matches = array_map(function($m) {
			return $m['match'];
		}, $matches);
	} else {
		preg_match_all(_PREG_MODELE, $flux, $matches, PREG_SET_ORDER);
	}
	$modeles_avec_notes = pipeline('modeles_avec_notes', array());

	foreach ($matches as $match) {
		if (in_array($match[1], $modeles_avec_notes)) {
			list($mod, $type, $id, $params, $fin) = array_pad($match, 5, '');
			$modele = inclure_modele($type, $id, $params, '');
			$flux = str_replace($match[0], $modele, $flux);
		}
	}
	return $flux;
}

/**
 * Retourne la liste des modèles à traiter de manière anticipée
 * Pour y gérer les notes
 * @param array $flux liste de modeles
 * @return $array $flux
 **/
function modeles_avec_notes_modeles_avec_notes($flux = array()) {
	$flux[] = 'liste_articles_auteurs';
	return $flux;
}
