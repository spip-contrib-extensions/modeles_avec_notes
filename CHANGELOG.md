# CHANGELOG

## 1.0.7 - 2024-05-07

### Fixed

- Compatible SPIP 4.y.z


### Removed

- Compatibilité SPIP 3.2
## 1.0.6 - 2023-09-02

### Fixed

- Pouvoir avoir plusieurs modèles avec notes différents dans un même article (bug en SPIP 4.2 seulement)
## 1.0.5 - 2023-02-27

### Fixed

- Compatible SPIP 4.2
